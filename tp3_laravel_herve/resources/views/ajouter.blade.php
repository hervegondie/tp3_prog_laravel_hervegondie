@extends('layout')
@section('title', 'Ajouter un Produit')
@section('content')


<h1 class="text-center">Ajout de produits</h1>
<div class="container is-max-widescreen">
<form action="post">
  <div class="file has-name">
    <label class="file-label">
      <input class="file-input" type="file" name="resume">
      <span class="file-cta">
        <span class="file-icon">
          <i class="fas fa-upload"></i>
        </span>
        <span class="file-label">
          Choisir une image…
        </span>
      </span>
      <span class="file-name">
        Screen Shot 2017-07-29 at 15.54.25.png
      </span>
    </label>
  </div>
  <div class="field">
    <label for="description" class="label">Description</label>
    <div class="control">
      <input class="input" type="text" name="description" id="description">
    </div>
  </div>

  <div class="field">
    <label class="label" for="date" class="label">Date</label>
    <div class="control">
      <input class="input" type="date" name="date" id="date">
    </div>
  </div>

  <div class="field">
    <label class="label" for="temperature">Temperature</label>
    <div class="control">
      <input class="input" type="text" name="temperature" id="temperature">
    </div>
  </div>

  <div class="buttons">
  <button class="button is-success">Soumettre</button>

</div>
</form>

</div>



@endsection