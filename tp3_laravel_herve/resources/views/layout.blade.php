<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="/resources/css/app.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
</head>
<body>
<header>
<nav class="navbar is-primary">
        <div class="navbar-menu ">
            <div class="navbar-start">
            <a href="/" class="navbar-item">Accueil</a>
            <a href="/ajouter" class="navbar-item">Ajouter un produit</a>
            </div>
            <div class="navbar-end">
            <a href="/profil" class="navbar-item">Profil</a>
            <a href="/connexion" class="navbar-item">Se Connecter</a>
            <a href="register" class="navbar-item">S'inscrire</a>
            <!-- <a href="register" class="navbar-item">S'inscrire</a> -->
            </div>
            
        </div>
    </nav>
</header>



<div>
@yield('content')
</div>

<section class="pt-4">
    <footer class=" page-footer font-small has-background-info">
    <p class="text-center text-uppercase">@2021</p>
    <p class="text-center">Hervé GONDIE et Julie BERNIER</p>
    </footer>
</section>

    
</body>
</html>