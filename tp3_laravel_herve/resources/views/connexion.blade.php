@extends('layout')
@section('title', 'Se Connecter')
@section('content')


<div>
    <h1 class="text-center">Connexion</h1>

    <form action="post" class="text-center">
        <!-- Email Address -->
    <div class="mt-4 m-4">
        <x-label for="email" :value="__('Email')" />

        <x-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required />
    </div>

    <!-- Password -->
    <div class="mt-4">
        <x-label for="password" :value="__('Password')" />

        <x-input id="password" class="block mt-1 w-full" type="password" name="password" required autocomplete="new-password" />
    </div>

    </form>
</div>


@endsection