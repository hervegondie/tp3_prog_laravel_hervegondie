@extends('layout')
@section('title', 'Profil')
@section('content')

<h2 class="text-center">Profils des utilisateurs</h3>
<div class="container">
<div class="row row-cols-1 row-cols-md-3">
  @foreach ($user as $user)
  <div class="card" style="width: 18rem;">
  <div class="card-body">
  <p class="card-text">Nom: {{$user->name}}</p>
  <p class="card-text">Adresse: {{$user->adress}}</p>
  <p class="card-text">Ville: {{$user->city}}</p>
  <p class="card-text">Courriel: {{$user->email}}</p>
  <a href="#" class="btn btn-primary">Modifier</a>
  </div>
  </div>
  @endforeach

  </div>
  </div>

@endsection