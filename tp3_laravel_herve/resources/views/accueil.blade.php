@extends('layout')
@section('title', 'Accueil')

@section('content')

<div class="container">
<div class="row row-cols-1 row-cols-md-3">
    @foreach ($food as $food)


    <div class="col mb-2">
    
        <div class="card h-100 bg-light" style="width: 25rem;">
            <img class="card-img-top" src="{{$food->image}}" alt="image card" >
            <div class="card-body">
                <div class="card-title">
                    <h4>Aliment: {{$food->description}}</h4>
                </div>
                <div class="card-text">
                    <p>Temperature: {{$food->méteo}}</p>
                    <p> Date de dépôt: {{$food->created_at}}</p>
                    <a href="#" class="btn btn-success">Réserver</a>
                </div>
            </div>
        </div>
        
    </div>




    @endforeach
</div>
</div>

@endsection