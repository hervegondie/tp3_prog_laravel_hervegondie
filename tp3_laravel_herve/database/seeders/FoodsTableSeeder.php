<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;


class FoodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('foods')->insert([
            'description' => 'Pain baguette',
            'image' => 'public\Images\pain.jpg',
            'méteo'=> 25,
            'user_id'=>1,
            'is_reserved' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('foods')->insert([
            'description' => 'Carotte',
            'image' => 'public\Images\carottes.jpg',
            'méteo'=> 25,
            'user_id'=>2,
            'is_reserved' => 0,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('foods')->insert([
            'description' => 'Tomates',
            'image' => 'public\Images\tomates.jpg',
            'méteo'=> 25,
            'user_id'=>1,
            'is_reserved' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('foods')->insert([
            'description' => 'Pommes',
            'image' => 'public\Images\pommes.jpg',
            'méteo'=> 25,
            'user_id'=>1,
            'is_reserved' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('foods')->insert([
            'description' => 'Avocats',
            'image' => 'public\Images\avocats.jpg',
            'méteo'=> 25,
            'user_id'=>1,
            'is_reserved' => 0,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('foods')->insert([
            'description' => 'Bananes',
            'image' => 'public\Images\bananes.jpg',
            'méteo'=> 25,
            'user_id'=>3,
            'is_reserved' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('foods')->insert([
            'description' => 'Pommes de terre',
            'image' => 'public\Images\pommes_de_terre.jpg',
            'méteo'=> 25,
            'user_id'=>5,
            'is_reserved' => 0,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('foods')->insert([
            'description' => 'Fraises',
            'image' => 'public\Images\fraies.jpg',
            'méteo'=> 25,
            'user_id'=>3,
            'is_reserved' => 0,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('foods')->insert([
            'description' => 'Lait',
            'image' => 'public\Images\lait.jpg',
            'méteo'=> 10,
            'user_id'=>4,
            'is_reserved' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('foods')->insert([
            'description' => 'Raisins',
            'image' => 'public\Images\raisins.jpg',
            'méteo'=> 15,
            'user_id'=>1,
            'is_reserved' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('foods')->insert([
            'description' => 'viandes',
            'image' => 'public\Images\viandes.jpg',
            'méteo'=> 15,
            'user_id'=>2,
            'is_reserved' => 0,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('foods')->insert([
            'description' => 'Viandes',
            'image' => 'public\Images\viandes2.jpg',
            'méteo'=> 15,
            'user_id'=>1,
            'is_reserved' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
