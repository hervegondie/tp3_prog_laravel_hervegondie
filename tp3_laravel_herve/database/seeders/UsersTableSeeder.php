<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Alain Michaud',
            'email' => 'user1@user.ca',
            'email_verified_at'=>Carbon::now()->format('Y-m-d H:i:s'),
            'adress'=> '255 rue de la gare, Montréal',
            'city'=> 'Québec',
            'food_id'=>1,
            'password' => bcrypt('123456'),
            'remember_token'=>'cegep Garneau',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('users')->insert([
            'name' => 'Paul Martin',
            'email' => 'user2@user2.ca',
            'email_verified_at'=>Carbon::now()->format('Y-m-d H:i:s'),
            'adress'=> '270 rue de Bigorre',
            'city'=> 'Québec',
            'food_id'=>2,
            'password' => bcrypt('123456'),
            'remember_token'=>'Charlesbourg',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('users')->insert([
            'name' => 'Manon Gagnon',
            'email' => 'user3@user3.ca',
            'email_verified_at'=>Carbon::now()->format('Y-m-d H:i:s'),
            'adress'=> '235 rue cliche',
            'city'=> 'Québec',
            'food_id'=>4,
            'password' => bcrypt('123456'),
            'remember_token'=>'Montmorency',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('users')->insert([
            'name' => 'Yvonne Tremblay',
            'email' => 'user4@user4.ca',
            'email_verified_at'=>Carbon::now()->format('Y-m-d H:i:s'),
            'adress'=> '3848 rue des Thuyas',
            'city'=> 'Québec',
            'food_id'=>4,
            'password' => bcrypt('123456'),
            'remember_token'=>'Lac Saint Charles',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('users')->insert([
            'name' => 'Charles Poltron',
            'email' => 'user5@user5.ca',
            'email_verified_at'=>Carbon::now()->format('Y-m-d H:i:s'),
            'adress'=> '1453 rue Émond',
            'city'=> 'Québec',
            'food_id'=>4,
            'password' => bcrypt('123456'),
            'remember_token'=>'Lac Saint Charles',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
