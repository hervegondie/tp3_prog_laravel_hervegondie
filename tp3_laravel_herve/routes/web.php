<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AccueilController;
use App\Http\Controllers\ProfilController;
use App\Http\Controllers\ConnexionController;
use App\Http\Controllers\AjoutController;
use App\Http\Controllers\InscriptionController;
use App\Http\Controllers\FoodController;
use App\Http\Controllers\UserController;








Route::resource('/', AccueilController::class);

Route::resource('/profil', ProfilController::class);  

Route::resource('/connexion', ConnexionController::class);   

Route::get('/ajouter', [AjoutController::class, 'index']);

Route::post('/ajouter', [AjoutController::class, 'store']);

Route::resource('/inscription', InscriptionController::class);

Route::get('/', [FoodController::class, 'index']);

Route::get('/profil', [UserController::class, 'index']);




// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');



require __DIR__.'/auth.php';